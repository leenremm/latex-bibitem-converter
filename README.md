# Script to convert LATEX reference from .bib file format to bibitem format.

## About

Author: Leen Remmelzwaal

Written: August 2019

License: CC BY

## Description

A Windows BAT script to convert LATEX reference from .bib file format to bibitem format.

## Getting Started

Install Latex editor for Windows.

## Getting Started

Copy the contents of your document's .bib file into the input.bib file in the repository.

Double click the file convert.bat

The results will appear in the file: output.bbl

## Acknowledgement

Source: https://tex.stackexchange.com/questions/124874/converting-to-bibitem-in-latex
